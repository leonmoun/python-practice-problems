# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.
s = ['a', 'b', 'c', 'c']
def remove_duplicate_letters(s):
    if not s:
        return ''
    used = set()
    result = []    
    for letters in s:
        if letters not in used:
            used.add(letters)
            result.append(letters)
    return ''.join(result)        
