# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
values = (1, 2, 3, 4, 5, 6, 7)
def find_second_largest(values):
    if len(values) < 2:
        return None

    largest = second_largest = float('-inf')

    for value in values:
        if value > largest:
            second_largest = largest
            largest = value
        elif value > second_largest and value < largest:
            second_largest = value
            
    if second_largest == float('-inf'):
        return None
    
    return second_largest
